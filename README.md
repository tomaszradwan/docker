# Docker

## PHP5.6, Nginx, Redis, MySQL, PhpMyAdmin, Visual Studio Code

#

## Xdebug Visual Studio Code
### Plik launch.json w Visual Studio Code

```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9000,
            "log": true,
            "externalConsole": false,
            "pathMappings": {
                "/var/www": "${workspaceRoot}/docker/www/"
            }
        },
        {
            "name": "Launch currently open script",
            "type": "php",
            "request": "launch",
            "program": "${file}",
            "cwd": "${fileDirname}",
            "port": 80,
            "pathMappings": {
                "/var/www": "${workspaceRoot}/docker/www/"
              }
        }
    ]
}
```


### Xdebug dla Visual Studio Code

https://stackoverflow.com/questions/52579102/debug-php-with-vscode-and-docker
https://github.com/felixfbecker/vscode-php-debug


### Xdebug dla PhpStorm

https://intellij-support.jetbrains.com/hc/en-us/community/posts/360000149204-phpStorm-xDebug-Docker-not-working-in-Windows-10


## REDIS - pomocne komendy/linki

Wykonać w kontenerze redis:
* redis-cli flushall - czyszczenie pamięci cache
* redis-cli --scan - wyświetlenie wszystkich kluczy w cache'u

https://redis.io/commands
https://czterytygodnie.pl/wprowadzenie-do-redis-a/


## BŁĘDY

https://stackoverflow.com/questions/15423500/nginx-showing-blank-php-pages
